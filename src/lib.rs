#[derive(Clone, Copy, Debug)]
pub struct Context {
    pos: Pos,
}

impl Default for Context {
    fn default() -> Self {
        Context {
            pos: Pos {
                line: 1,
                character: 1,
            },
        }
    }
}

impl Context {
    pub fn newline(&mut self) {
        self.pos.line += 1;
        self.pos.character = 1;
    }
    pub fn n(&mut self, n: usize) {
        self.pos.character += n;
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Pos {
    pub line: usize,
    pub character: usize,
}

#[derive(Clone, Copy, Debug)]
pub struct Span {
    pub start: Pos,
    pub end: Pos,
}

#[derive(Clone, Copy, Debug)]
pub struct Error {
    pub span: Span,
    pub message: &'static str,
}

pub trait Def: Sized {
    const NAME: &'static str;
    fn parse(s: &mut &str, ctx: &mut Context) -> Result<Self, Error> {
        if s.len() == 0 {
            return Err(Error {
                span: Span {
                    start: ctx.pos,
                    end: ctx.pos,
                },
                message: Self::NAME,
            });
        }
        let start = ctx.pos;
        Self::impl_parse(s, ctx).ok_or_else(|| {
            let end = ctx.pos;
            Error {
                span: Span { start, end },
                message: Self::NAME,
            }
        })
    }
    fn try_parse(s: &mut &str, ctx: &mut Context) -> Option<Self> {
        if s.len() == 0 {
            return None;
        }
        let oldstr = *s;
        let start = ctx.pos;
        match Self::impl_parse(s, ctx) {
            Some(t) => Some(t),
            None => {
                ctx.pos = start;
                *s = oldstr;
                None
            }
        }
    }
    fn parse_all(s: &mut &str, ctx: &mut Context) -> Result<Self, Error> {
        Self::parse(s, ctx).and_then(|r| {
            if s.is_empty() {
                Ok(r)
            } else {
                Err(Error {
                    span: Span {
                        start: ctx.pos,
                        end: ctx.pos,
                    },
                    message: Self::NAME,
                })
            }
        })
    }
    fn impl_parse(s: &mut &str, ctx: &mut Context) -> Option<Self>;
    fn check(s: &mut str) -> bool;
    fn encode(&self, buf: &mut String);
}

pub mod parser;
