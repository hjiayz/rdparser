use crate::*;

pub struct Document {
    lines:Vec<Line>,
}

impl Def for Document {
    const NAME: &'static str = "document";

    fn impl_parse(s: &mut &str, ctx: &mut Context) -> Option<Self> {
        let mut document = Document {
            lines:vec![]
        };
        while let Some(line) = Line::try_parse(s,ctx) {
            document.lines.push(line);
            if Eol::try_parse(s,ctx).is_none() {
                break;
            }
        }
        document
    }

    fn check(s: &mut str) -> bool {
        while s.len()>0 {
            if Line::check(s)==false {
                return false;
            }
            if Eol::check(s)==false {
                break;
            }
        }
        true
    }

    fn encode(&self, buf: &mut String) {
        for line in self.lines {
            line.encode(buf);
        }
    }
    

}